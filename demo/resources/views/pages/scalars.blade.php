<!doctype html>
<html lang="en">
<head>
    <title>PHP7 Demo - {{ $title }}</title>
</head>
<body>
<h3>Scalar integer value: {{$typeHint}}</h3>
<h3>Returned string: {{$returnType}}</h3>

<h4>What's going on here:</h4>
<p>Below is some sample code of PHP 7.0's Scalar Type Hinting and Return Type Declarations. Both of which are particularly useful additions to the language - PHP is still a loosely typed language, but with Scalar Type Hints and Return Types, we can be more verbose about what our code does and intends to handle.</p>

<!-- HTML generated using hilite.me --><div style="background: #272822; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #75715e">&lt;?php</span>

<span style="color: #66d9ef">namespace</span> <span style="color: #a6e22e">App\Http\Controllers</span><span style="color: #f8f8f2">;</span>

<span style="color: #66d9ef">class</span> <span style="color: #a6e22e">DemoController</span> <span style="color: #66d9ef">extends</span> <span style="color: #a6e22e">Controller</span>
<span style="color: #f8f8f2">{</span>
    <span style="color: #66d9ef">public</span> <span style="color: #66d9ef">function</span> <span style="color: #a6e22e">index</span><span style="color: #f8f8f2">()</span>
    <span style="color: #f8f8f2">{</span>
        <span style="color: #66d9ef">return</span> <span style="color: #a6e22e">view</span><span style="color: #f8f8f2">(</span><span style="color: #e6db74">&#39;pages.index&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;title&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Index&#39;</span><span style="color: #f8f8f2">]);</span>
    <span style="color: #f8f8f2">}</span>


    <span style="color: #66d9ef">public</span> <span style="color: #66d9ef">function</span> <span style="color: #a6e22e">scalarTypes</span><span style="color: #f8f8f2">()</span>
    <span style="color: #f8f8f2">{</span>
        <span style="color: #f8f8f2">$returnType</span> <span style="color: #f92672">=</span> <span style="color: #f8f8f2">$this</span><span style="color: #f92672">-&gt;</span><span style="color: #a6e22e">returnTypeDeclaration</span><span style="color: #f8f8f2">();</span>
        <span style="color: #f8f8f2">$typeHint</span> <span style="color: #f92672">=</span> <span style="color: #f8f8f2">$this</span><span style="color: #f92672">-&gt;</span><span style="color: #a6e22e">scalarTypeHint</span><span style="color: #f8f8f2">(</span><span style="color: #ae81ff">5</span><span style="color: #f8f8f2">);</span>

        <span style="color: #66d9ef">return</span> <span style="color: #a6e22e">view</span><span style="color: #f8f8f2">(</span><span style="color: #e6db74">&#39;pages.scalars&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;title&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Scalar Type Hints / Return Type Declarations&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;returnType&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #f8f8f2">$returnType,</span> <span style="color: #e6db74">&#39;typeHint&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #f8f8f2">$typeHint]);</span>
    <span style="color: #f8f8f2">}</span>

    <span style="color: #66d9ef">protected</span> <span style="color: #66d9ef">function</span> <span style="color: #a6e22e">returnTypeDeclaration</span><span style="color: #f8f8f2">()</span><span style="color: #f92672">:</span> <span style="color: #a6e22e">string</span>
    <span style="color: #f8f8f2">{</span>
        <span style="color: #66d9ef">return</span> <span style="color: #e6db74">&#39;This is a string!&#39;</span><span style="color: #f8f8f2">;</span>
    <span style="color: #f8f8f2">}</span>

    <span style="color: #66d9ef">protected</span> <span style="color: #66d9ef">function</span> <span style="color: #a6e22e">scalarTypeHint</span><span style="color: #f8f8f2">(</span><span style="color: #a6e22e">int</span> <span style="color: #f8f8f2">$id)</span>
    <span style="color: #f8f8f2">{</span>
        <span style="color: #66d9ef">return</span> <span style="color: #e6db74">&quot;We got back an integer value: {</span><span style="color: #f8f8f2">$id</span><span style="color: #e6db74">}&quot;</span><span style="color: #f8f8f2">;</span>
    <span style="color: #f8f8f2">}</span>
<span style="color: #f8f8f2">}</span>
</pre></div>

</body>
</html>