<!doctype html>
<html lang="en">
    <head>
        <title>PHP7 Demo - {{ $title }}</title>
    </head>
    <body>
        <h4>Demos here:</h4>
        <a href="./spaceShipSort/">Space Ship Operator (Sorting)</a><br>
        <a href="./scalars/">Scalar Type Hints / Return Type Declarations</a><br>

        <h4>What is this:</h4>
        <p>This is just a derpy site I built to test some of the new features of PHP 7.
            We're using Nginx and PHP-FPM on Debian Jessie. The code is assisted by the <a href="http://lumen.laravel.com/">Lumen micro framework</a>.
            If you're seeing this it means none of the moving parts blew up. Hooray!
        </p>
    </body>
</html>
