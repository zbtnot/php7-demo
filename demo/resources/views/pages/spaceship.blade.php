<!doctype html>
<html lang="en">
    <head>
        <title>PHP7 Demo - {{ $title }}</title>
        <link rel="stylesheet" href="/css/style.css" type="text/css">
    </head>
    <body>
        <h4>Sort Types:</h4>
        <a href="./name">Name</a>
        <a href="./tickets">Open Tickets</a>
        <a href="./hired">Hire Date</a>
        <br><br>
        <h4>Sample data sorted:</h4>
        <table>
            <thead>
                <tr>
                    @foreach ($employeeHeaders as $header)
                        <th>{{$header}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)
                    <tr>
                        <td>{{$employee['name']}}</td>
                        <td>{{$employee['openTickets']}}</td>
                        <td>{{$employee['hired']}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <h4>What's going on here:</h4>
        <p>
            New in PHP 7.0 is the combined comparison (or spaceship) operator.
            This allows for <abbr title="Don't Repeat Yourself">DRY</abbr>-er code.
            See below for an example from this page:
        </p>

        <!-- HTML generated using hilite.me --><div style="background: #272822; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #75715e">&lt;?php</span>
<span style="color: #66d9ef">class</span> <span style="color: #a6e22e">DemoController</span> <span style="color: #66d9ef">extends</span> <span style="color: #a6e22e">Controller</span>
<span style="color: #f8f8f2">{</span>
    <span style="color: #66d9ef">public</span> <span style="color: #66d9ef">function</span> <span style="color: #a6e22e">index</span><span style="color: #f8f8f2">()</span>
    <span style="color: #f8f8f2">{</span>
        <span style="color: #66d9ef">return</span> <span style="color: #a6e22e">view</span><span style="color: #f8f8f2">(</span><span style="color: #e6db74">&#39;pages.index&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;title&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Index&#39;</span><span style="color: #f8f8f2">]);</span>
    <span style="color: #f8f8f2">}</span>

    <span style="color: #66d9ef">public</span> <span style="color: #66d9ef">function</span> <span style="color: #a6e22e">spaceShipSort</span><span style="color: #f8f8f2">($sortBy)</span>
    <span style="color: #f8f8f2">{</span>
        <span style="color: #f8f8f2">$employees</span> <span style="color: #f92672">=</span> <span style="color: #f8f8f2">[</span>
            <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;name&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Giorgio&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;openTickets&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #ae81ff">900</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;hired&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;2013-11-13&#39;</span><span style="color: #f8f8f2">],</span>
            <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;name&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Ryan&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;openTickets&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #ae81ff">100</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;hired&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;2010-01-01&#39;</span><span style="color: #f8f8f2">],</span>
            <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;name&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Nathan&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;openTickets&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #ae81ff">12345</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;hired&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;1992-07-23&#39;</span><span style="color: #f8f8f2">],</span>
        <span style="color: #f8f8f2">];</span>
        <span style="color: #f8f8f2">$employeeKeys</span> <span style="color: #f92672">=</span> <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;Name&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;Open Tickets&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;Hired&#39;</span><span style="color: #f8f8f2">];</span>

        <span style="color: #66d9ef">switch</span> <span style="color: #f8f8f2">($sortBy)</span> <span style="color: #f8f8f2">{</span>
            <span style="color: #66d9ef">case</span> <span style="color: #e6db74">&#39;hired&#39;</span><span style="color: #f92672">:</span>
                <span style="color: #f8f8f2">usort($employees,</span> <span style="color: #66d9ef">function</span> <span style="color: #f8f8f2">($a,</span> <span style="color: #f8f8f2">$b)</span> <span style="color: #f8f8f2">{</span>
                    <span style="color: #66d9ef">return</span> <span style="color: #f8f8f2">(</span><span style="color: #66d9ef">new</span> <span style="color: #a6e22e">\DateTime</span><span style="color: #f8f8f2">($a[</span><span style="color: #e6db74">&#39;hired&#39;</span><span style="color: #f8f8f2">]))</span> <span style="color: #f92672">&lt;=&gt;</span> <span style="color: #f8f8f2">(</span><span style="color: #66d9ef">new</span> <span style="color: #a6e22e">\DateTime</span><span style="color: #f8f8f2">($b[</span><span style="color: #e6db74">&#39;hired&#39;</span><span style="color: #f8f8f2">]));</span>
                <span style="color: #f8f8f2">});</span>
                <span style="color: #66d9ef">break</span><span style="color: #f8f8f2">;</span>
            <span style="color: #66d9ef">case</span> <span style="color: #e6db74">&#39;tickets&#39;</span><span style="color: #f92672">:</span>
                <span style="color: #f8f8f2">usort($employees,</span> <span style="color: #66d9ef">function</span> <span style="color: #f8f8f2">($a,</span> <span style="color: #f8f8f2">$b)</span> <span style="color: #f8f8f2">{</span>
                    <span style="color: #66d9ef">return</span> <span style="color: #f8f8f2">$a[</span><span style="color: #e6db74">&#39;openTickets&#39;</span><span style="color: #f8f8f2">]</span> <span style="color: #f92672">&lt;=&gt;</span> <span style="color: #f8f8f2">$b[</span><span style="color: #e6db74">&#39;openTickets&#39;</span><span style="color: #f8f8f2">];</span>
                <span style="color: #f8f8f2">});</span>
                <span style="color: #66d9ef">break</span><span style="color: #f8f8f2">;</span>
            <span style="color: #66d9ef">case</span> <span style="color: #e6db74">&#39;name&#39;</span><span style="color: #f92672">:</span> <span style="color: #75715e">// intentional fall-through</span>
            <span style="color: #66d9ef">default</span><span style="color: #f92672">:</span>
                <span style="color: #f8f8f2">usort($employees,</span> <span style="color: #66d9ef">function</span> <span style="color: #f8f8f2">($a,</span> <span style="color: #f8f8f2">$b)</span> <span style="color: #f8f8f2">{</span>
                    <span style="color: #66d9ef">return</span> <span style="color: #f8f8f2">$a[</span><span style="color: #e6db74">&#39;name&#39;</span><span style="color: #f8f8f2">]</span> <span style="color: #f92672">&lt;=&gt;</span> <span style="color: #f8f8f2">$b[</span><span style="color: #e6db74">&#39;name&#39;</span><span style="color: #f8f8f2">];</span>
                <span style="color: #f8f8f2">});</span>
                <span style="color: #66d9ef">break</span><span style="color: #f8f8f2">;</span>
        <span style="color: #f8f8f2">}</span>
        <span style="color: #66d9ef">return</span> <span style="color: #a6e22e">view</span><span style="color: #f8f8f2">(</span><span style="color: #e6db74">&#39;pages.spaceship&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #f8f8f2">[</span><span style="color: #e6db74">&#39;title&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #e6db74">&#39;Sorting with the Spaceship Operator &lt;=&gt;&#39;</span><span style="color: #f8f8f2">,</span> <span style="color: #e6db74">&#39;employees&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #f8f8f2">$employees,</span> <span style="color: #e6db74">&#39;employeeHeaders&#39;</span> <span style="color: #f92672">=&gt;</span> <span style="color: #f8f8f2">$employeeKeys]);</span>
    <span style="color: #f8f8f2">}</span>
<span style="color: #f8f8f2">}</span>
</pre></div>


    </body>
</html>