<?php
/**
 * Created by IntelliJ IDEA.
 * User: gio
 * Date: 11/22/15
 * Time: 3:28 PM
 */

namespace App\Http\Controllers;

class DemoController extends Controller
{
    public function index()
    {
        return view('pages.index', ['title' => 'Index']);
    }

    public function spaceShipSort($sortBy)
    {
        $employees = [
            ['name' => 'Giorgio', 'openTickets' => 900, 'hired' => '2013-11-13'],
            ['name' => 'Ryan', 'openTickets' => 100, 'hired' => '2010-01-01'],
            ['name' => 'Nathan', 'openTickets' => 12345, 'hired' => '1992-07-23'],
        ];
        $employeeKeys = ['Name', 'Open Tickets', 'Hired'];

        switch ($sortBy) {
            case 'hired':
                usort($employees, function ($a, $b) {
                    return (new \DateTime($a['hired'])) <=> (new \DateTime($b['hired']));
                });
                break;
            case 'tickets':
                usort($employees, function ($a, $b) {
                    return $a['openTickets'] <=> $b['openTickets'];
                });
                break;
            case 'name': // intentional fall-through
            default:
                usort($employees, function ($a, $b) {
                    return $a['name'] <=> $b['name'];
                });
                break;
        }
        return view('pages.spaceship', ['title' => 'Sorting with the Spaceship Operator <=>', 'employees' => $employees, 'employeeHeaders' => $employeeKeys]);
    }

    public function scalarTypes()
    {
        $returnType = $this->returnTypeDeclaration();
        $typeHint = $this->scalarTypeHint(5);

        return view('pages.scalars', ['title' => 'Scalar Type Hints / Return Type Declarations', 'returnType' => $returnType, 'typeHint' => $typeHint]);
    }

    protected function returnTypeDeclaration(): string
    {
        return 'This is a string!';
    }

    protected function scalarTypeHint(int $id)
    {
        return "We got back an integer value: {$id}";
    }
}