<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'DemoController@index');

$app->get('/spaceShipSort/', function () {
    // lumen doesn't support optional parameters for routes
    $controller = new \App\Http\Controllers\DemoController();
    return $controller->spaceShipSort('name');
});
$app->get('/spaceShipSort/{sortType}', 'DemoController@spaceShipSort');
$app->get('/scalars', 'DemoController@scalarTypes');
