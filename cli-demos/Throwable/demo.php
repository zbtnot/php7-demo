<?php
function ShouldReturnADataBaseHandle()
{
    return null;
}

try {
    $db = ShouldReturnADataBaseHandle();
    $stmt = $db->prepare('SELECT * FROM users');
} catch (Throwable $t) {
    echo "Oops, something went wrong: {$t->getMessage()}";
}
