<?php
require './vendor/autoload.php';

use \Foo\Bar\{ Baz, Biz, Buz as Buzz };

$baz = new Baz();
$biz = new Biz();
$buz = new Buzz();

echo $baz->foo() . "\n";
echo $biz->foo() . "\n";
echo $buz->foo() . "\n";