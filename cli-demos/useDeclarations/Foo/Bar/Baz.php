<?php
/**
 * Created by IntelliJ IDEA.
 * User: garcamone
 * Date: 11/30/15
 * Time: 11:50 AM
 */

namespace Foo\Bar;


class Baz implements Fooable
{
    public function foo()
    {
        return 'This is a baz implementation';
    }
}