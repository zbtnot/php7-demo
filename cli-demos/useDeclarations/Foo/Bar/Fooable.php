<?php
/**
 * Created by IntelliJ IDEA.
 * User: garcamone
 * Date: 11/30/15
 * Time: 11:51 AM
 */

namespace Foo\Bar;

interface Fooable
{
    public function foo();
}