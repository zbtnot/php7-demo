# php7-demo

A demo of some PHP 7 features (cli and php-fpm with nginx) on a Debian Jessie vagrantbox

## How to
1. clone the repo
2. cd into it
3. run vagrant up

## Features
- nginx is listening on 127.0.0.1:8080
    - there's a couple of demos built using Lumen
- also some CLI demos
    - vagrant ssh into the vm
    - located in /vagrant/cli-demos